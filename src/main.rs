use clap::Parser;
use std::path::PathBuf;

#[derive(Debug, Clone, clap::Parser)]
#[command(author, version, about)]
struct Arguments {
    paths: Vec<PathBuf>,
}

fn main() -> anyhow::Result<()> {
    let args = Arguments::parse();

    for path in &args.paths {
        match path.exists() {
            false => eprintln!("{}: {}: path does not exists", env!("CARGO_PKG_NAME"), path.display()),
            true => trash::delete(path)?,
        }
    }

    Ok(())
}
